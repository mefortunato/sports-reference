FROM python:3.9-slim as build

WORKDIR /build

COPY . .

RUN pip install -r requirements.txt