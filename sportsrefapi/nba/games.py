import asyncio
import httpx
import json
import pandas as pd
import string
from bs4 import BeautifulSoup
from typing import Union

season_months = ['october', 'november', 'december',
                 'january', 'february', 'march', 'april', 'may', 'june']


async def get_boxscore(url: str):
    async with httpx.AsyncClient() as client:
        resp = await client.get(url, timeout=None)
    basic_dfs = pd.read_html(resp.text, match='Basic Box Score Stats')
    away_basic_df = pd.to_numeric(
        basic_dfs[0].iloc[-1].droplevel(0).drop(['Starters', '+/-'])
    )
    home_basic_df = pd.to_numeric(
        basic_dfs[len(basic_dfs) //
                  2].iloc[-1].droplevel(0).drop(['Starters', '+/-'])
    )
    away_adv_df, home_adv_df = pd.read_html(
        resp.text, match='Advanced Box Score Stats')
    away_adv_df = pd.to_numeric(
        away_adv_df.iloc[-1].droplevel(0).drop(['Starters', 'BPM'])
    )
    home_adv_df = pd.to_numeric(
        home_adv_df.iloc[-1].droplevel(0).drop(['Starters', 'BPM'])
    )
    return {
        'away': away_basic_df,
        'home': home_basic_df,
        'away_adv': away_adv_df,
        'home_adv': home_adv_df
    }


async def get_games(year: int):
    urls = [
        f'https://www.basketball-reference.com/leagues/NBA_{year}_games-{month}.html'
        for month in season_months
    ]
    async with httpx.AsyncClient() as client:
        resps = [client.get(url) for url in urls]
        game_pages_html = await asyncio.gather(*resps)
    game_pages_html = filter(lambda x: x.status_code == 200, game_pages_html)
    game_pages_html = [page.text for page in game_pages_html]
    soups = [BeautifulSoup(page, 'lxml') for page in game_pages_html]
    box_score_tds = []
    for soup in soups:
        box_score_tds.extend(soup.find_all(
            'td', {'data-stat': 'box_score_text'}
        ))
    box_score_urls = [
        td.find('a')['href']
        for td in box_score_tds if td.find('a')
    ]
    boxscores = await asyncio.gather(*[
        get_boxscore('https://www.basketball-reference.com'+url) for url in box_score_urls
    ])
    game_dfs = pd.concat([
        pd.read_html(page)[0]
        for page in game_pages_html
    ])
    game_dfs = game_dfs.rename(columns={
        'Start (ET)': 'Time',
        'Visitor/Neutral': 'Away',
        'PTS': 'Away PTS',
        'Home/Neutral': 'Home',
        'PTS.1': 'Home PTS',
        'Attend.': 'Attendance',
    })
    game_dfs['Date'] = pd.to_datetime(game_dfs['Date'])
    game_dfs['Away PTS'] = pd.to_numeric(game_dfs['Away PTS'], errors='coerce')
    game_dfs['Home PTS'] = pd.to_numeric(game_dfs['Home PTS'], errors='coerce')
    unnamed = [col for col in game_dfs.columns if 'Unnamed' in col]
    game_dfs = game_dfs.drop(columns=unnamed)
    game_dfs = game_dfs.dropna(subset=['Away PTS', 'Home PTS'])
    # return game_dfs, boxscores
    return game_dfs, boxscores
